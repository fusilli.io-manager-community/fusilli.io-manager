#!/bin/bash

stop() {
    printf "\rCTRL-C.. exiting      "
    docker-compose down
    mv ~/.ssh/known_hosts.bk ~/.ssh/known_hosts
    exit
}

trap 'stop' SIGINT

cp ~/.ssh/known_hosts ~/.ssh/known_hosts.bk

ssh-keygen -R localhost

docker-compose up -d --force-recreate --renew-anon-volumes


sleep 3

chmod 600 rsa/id_rsa
sftp -o StrictHostKeyChecking=no -i rsa/id_rsa -b putdata.txt -P 22 fusilli_ftp@localhost

python3 json_handler.py
