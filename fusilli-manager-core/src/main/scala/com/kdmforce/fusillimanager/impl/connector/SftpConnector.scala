package com.kdmforce.fusillimanager.impl.connector

import java.net.InetAddress

import akka.stream.IOResult
import akka.stream.alpakka.ftp.scaladsl.Sftp
import akka.stream.alpakka.ftp.{FtpCredentials, SftpSettings}
import akka.util.ByteString
import com.kdmforce.fusillimanager.api.EndpointSerializer.{Entity, Repo}
import com.kdmforce.fusillimanager.impl.connectors.Connector

import scala.concurrent.Future


object SftpConnector extends Connector[ByteString]{



  override def getSource(repo: Repo, entity: Entity) = {

    val settings = spawnConnection(repo)

    Sftp.fromPath(repo.schema_path + entity.entity_name, settings)

  }

  override def getSink(repo: Repo, entity: Entity) = {

    val settings = spawnConnection(repo)

    Sftp.toPath(repo.schema_path + entity.entity_name, settings)

  }


  def spawnConnection (repo: Repo): SftpSettings = {
    val credentials = FtpCredentials.create(repo.repo_user, repo.repo_password)

    SftpSettings
      .create(InetAddress.getByName(repo.host))
      .withPort(repo.port)
      .withCredentials(credentials)
      .withStrictHostKeyChecking(false)
  }
}
