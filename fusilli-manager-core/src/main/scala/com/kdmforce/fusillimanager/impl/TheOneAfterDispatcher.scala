package com.kdmforce.fusillimanager.impl

import akka.actor.{Actor, ActorRef, PoisonPill, Props, Terminated}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Repo, Job, Pipeline, PipelineStop}
import com.kdmforce.fusillimanager.impl.MessageSerializer.{JobMessage, LogMessage, PipelineStart, PipelineEnd}
import com.kdmforce.fusillimanager.impl.connector.{ConnectorIn, ConnectorOut}
import com.kdmforce.fusillimanager.impl.job._
import com.kdmforce.fusillimanager.impl.job.join_connections.BigSpawner
import play.api.libs.json.{Format, Json, JsValue,JsArray, JsObject}
import com.kdmforce.fusillimanager.impl.job.job_specific_objects.MatcherItems

object TheOneAfterDispatcher {
  def props = Props[TheOneAfterDispatcher]
}

class TheOneAfterDispatcher(pipeId: Int) extends Actor {

  private var children = 0

  private val log = Logging.getLogger(context.system, this)

  val logger = context.system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = "/user/logger",
      settings = ClusterSingletonProxySettings(context.system)),
    )
  var pipelineInputs = Map[Int, List[ActorRef]] ()
  

  override def receive ={
    case pipeline : Pipeline =>
      val grahmOut = Grahmizer.grahmize(pipeline.jobs)
      var whereTheArcsGo = grahmOut._2 //mappa arco entrante -> id job successivo
      val revScheduling :List [Job] = reverseAndIdentify(grahmOut._1, pipeline.jobs)
      var identities = Map[Int, ActorRef] () //mappa id ->attore


      for (repo <- pipeline.outputs){

        for (entity <- repo.entities){
          val newConnettore :ActorRef = context.system.actorOf(Props(new ConnectorOut(pipeId, repo, entity)))

          identities += (entity.entity_id -> newConnettore)
          whereTheArcsGo += (entity.connection_edge -> entity.entity_id)
        }

      }

      for ( j<- revScheduling ) {

        var followers = List[ActorRef]()

        for (f <- j.output_edges ){

          followers = followers.appended(identities(whereTheArcsGo(f)))
        }


        identities += (j.job_configuration_id -> jobMap (pipeId, j,followers))
      }




      var inJobs = List[ActorRef]()
      for (repo <- pipeline.inputs){

        for (entity <- repo.entities){
          val newConnettore :ActorRef = context.system.actorOf(Props(classOf[ConnectorIn], pipeId, repo, entity,identities(whereTheArcsGo(entity.connection_edge) )))
          inJobs = inJobs.appended(newConnettore)

          identities += (entity.entity_id -> newConnettore)
        }

        children = identities.map(elem => context.watch(elem._2)).size

        logger ! (LogMessage(pipeId, JobMessage("START", "START PIPELINE", 0)))
        pipeline.inputs.flatMap(
          repo => repo.entities.map(
            entity => identities(entity.entity_id) ! PipelineStart
          )
        )

      }
      pipelineInputs += (pipeline.pipeline_id -> inJobs)
      
    case Terminated(_) =>
        children -=1
        if (children == 0) {
          logger ! (LogMessage(pipeId, JobMessage("END", "END PIPELINE", 0)))
          self ! PoisonPill
        }

    case stopKey : PipelineStop =>
      //println("\n\n\n\n THE ONE AFTER " + stopKey)
      val stopId = stopKey.pipeline_id
      logger ! (LogMessage(pipeId, JobMessage("END", "STOPPING PIPELINE", 0)))
      for (ref <- pipelineInputs(stopId)){
        //println("\n\n\n\n SENDING TO " + ref)
        ref ! PipelineEnd
      }
    
      self ! PoisonPill

  }

  def reverseAndIdentify(gramout: List[Int], jobs: Seq[Job]) : List[Job] = {
    var idMap = Map[Int,Job]()
    var outList =List[Job]()
    for (j<-jobs) {
      idMap+=(j.job_configuration_id->j)
    }


    val revGram =gramout.reverse

    for (id<-revGram) {

      outList = outList.appended(idMap(id))
    }
    outList
  }


  def jobMap(instance: Int, job: Job, followers: List[ActorRef]) : ActorRef = {

    job.job_name match {
      case "concat" =>
        context.system.actorOf(Props(classOf[Concatter], instance, job,  followers(0)))

      case "rename" =>
        context.system.actorOf(Props(classOf[Renamer], instance, job,  followers(0)))

      case "replace" =>
        context.system.actorOf(Props(classOf[Replacer], instance, job,  followers(0)))

      case "split" =>
        context.system.actorOf(Props(classOf[Splitter], instance, job,  followers(0)))

      case "id" =>
        context.system.actorOf(Props(classOf[Mockingbird], instance, job, job.input_edges.size,  followers))
        
      case "join" =>
        //crea connettore
        val dbConnection = BigSpawner.spawnConnection(((job.job_configs(0).config).apply(0)\"secondary_input").get.as[Repo] )
        context.system.actorOf(Props(classOf[Joiner], instance, job,  followers(0), followers(1), dbConnection))

      case "match" =>
        val cond = MatcherItems.PredicateParser(job.job_configs(0).config)
        context.system.actorOf(Props(classOf[Matcher], instance, job,  followers(0), followers(1), cond))

      case "split&clone" =>
        context.system.actorOf(Props(classOf[SplitCloner], instance, job,  followers(0)))

      case "mark" =>
        context.system.actorOf(Props(classOf[Marker], instance, job, (job.job_configs(0).config\"new_key").get.as[String], (job.job_configs(0).config\"new_value").get.as[String], followers(0)))

      case "stack" =>
        context.system.actorOf(Props(classOf[Stacker], instance, job,  followers(0)))
    }
  }
}


