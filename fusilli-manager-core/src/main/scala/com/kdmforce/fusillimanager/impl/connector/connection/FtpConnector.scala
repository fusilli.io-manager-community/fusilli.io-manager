package com.kdmforce.fusillimanager.impl.connector.connection

import java.net.InetAddress
import akka.stream.IOResult
import akka.stream.alpakka.ftp.scaladsl.Ftp
import akka.stream.alpakka.ftp.{FtpCredentials, FtpSettings}
import akka.util.ByteString
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Entity, Repo}
import com.kdmforce.fusillimanager.impl.connector.Connector
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.Future


object FtpConnector extends Connector[ByteString]{

  private final val log: Logger = LoggerFactory.getLogger("FTP")

  override def getSource(repo: Repo, entity: Entity) = {
    log.info("START METHOD GETSOURCE")
    val file = repo.schema_path + entity.entity_name

    log.info("START CONNECTION")
    val settings = spawnConnection(repo)

    log.info(s"FILE CONNECTION ${file}")
    Ftp.fromPath(file, settings)
  }

  override def getSink(repo: Repo, entity: Entity) = {
    log.info("START METHOD GETSINK")
    val file = repo.schema_path + entity.entity_name

    log.info("START CONNECTION")
    val settings = spawnConnection(repo)

    log.info(s"FILE CONNECTION ${file}")
    Ftp.toPath(file, settings)
  }


  def spawnConnection (repo: Repo): FtpSettings = {
    val credentials = FtpCredentials.create(repo.repo_user, repo.repo_password)

    FtpSettings
      .create(InetAddress.getByName(repo.host))
      .withPort(repo.port)
      .withCredentials(credentials)
      .withBinary(true)
      .withPassiveMode(true)
  }
}
