package com.kdmforce.fusillimanager.impl

import com.lightbend.lagom.scaladsl.api.ServiceLocator
import com.lightbend.lagom.scaladsl.api.ServiceLocator.NoServiceLocator
import com.lightbend.lagom.scaladsl.server._
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import play.api.libs.ws.ahc.AhcWSComponents
import com.kdmforce.fusillimanager.api.FusilliManagerService
import com.softwaremill.macwire._
import com.lightbend.lagom.scaladsl.cluster._
import com.lightbend.lagom.scaladsl.playjson.{EmptyJsonSerializerRegistry, JsonSerializerRegistry}
import akka.management.scaladsl.AkkaManagement
import akka.management.cluster.bootstrap.ClusterBootstrap

class FusilliManagerLoader extends LagomApplicationLoader {

  override def load(context: LagomApplicationContext): LagomApplication =
    new FusilliManagerApplication(context) {
    // // Akka Management hosts the HTTP routes used by bootstrap
    // AkkaManagement(actorSystem).start()

    // // Starting the bootstrap process needs to be done explicitly
    // ClusterBootstrap(actorSystem).start()

      override def serviceLocator: ServiceLocator = NoServiceLocator
    }

  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new FusilliManagerApplication(context) with LagomDevModeComponents

  override def describeService = Some(readDescriptor[FusilliManagerService])
}

abstract class FusilliManagerApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with ClusterComponents
    with AhcWSComponents {

  // Bind the service that this server provides
  override lazy val lagomServer: LagomServer = serverFor[FusilliManagerService](wire[FusilliManagerServiceImpl])

  override lazy val jsonSerializerRegistry = MyRegistry

}
