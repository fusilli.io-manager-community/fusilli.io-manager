package com.kdmforce.fusillimanager.impl.job

import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import akka.cluster.singleton.{ClusterSingletonProxy, ClusterSingletonProxySettings}
import akka.event.Logging
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.Job
import com.kdmforce.fusillimanager.impl.MessageSerializer._

object Marker {
  def props = Props[Marker]
}

class Marker(instance: Int, job: Job, newKey : String, newValue : String, follower: ActorRef ) extends Actor {
  var index = 0

  private val log = Logging.getLogger(context.system, this)

  val logger = context.system.actorOf(
    ClusterSingletonProxy.props(
      singletonManagerPath = "/user/logger",
      settings = ClusterSingletonProxySettings(context.system)),
  )

  override def receive = {
    case PipelineStart =>
      log.info("START MARKER")
      logger ! (LogMessage(instance, JobMessage("START", "", job.job_configuration_id)))
      follower ! PipelineStart 

    case PipelineEnd =>
      log.info("END MARKER")
      logger ! (LogMessage(instance, JobMessage("END", "", job.job_configuration_id)))
      follower ! PipelineEnd 
      self ! PoisonPill


    case PipelineError(ex) =>
      log.error(ex, "ERROR ON MARKER")
      logger ! (LogMessage(instance, JobMessage("ERROR", ex.getMessage, job.job_configuration_id)))
      follower ! PipelineError(ex)
      self ! PoisonPill

    case el: Map[String, String] =>
      follower ! el + (newKey -> newValue)
  }

}
