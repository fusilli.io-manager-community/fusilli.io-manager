package com.kdmforce.fusillimanager.impl.connector

import akka.stream.scaladsl.{Flow, Source}
import akka.util.ByteString
import com.kdmforce.fusillimanager.api.EndpointSerializer.Entity
import com.kdmforce.fusillimanager.impl.connectors.DataFile
import play.api.libs.json.{JsValue, Json}

object JsonFile extends DataFile[ByteString, String] {
  override def formatInput(entity: Entity) = {

    val outputMap = mapJsonByteToMap(entity) _

    Flow[ByteString]
      .map(outputMap)

  }

  override def formatOutput(entity: Entity) = {

    val header = entity.attributes.map(_.attr_name)

    Flow[Map[String, String]]
      .map(mapToJsonString _)

  }

  def mapToJsonString (doc : Map[String, Any]) : ByteString ={
    var output = "{"
    var bool = 1
    for ((key,value)<-doc){
      if (bool ==1){bool =0 }
      else {output = output + ", "}
      output = output +"\"" + key + "\" : \"" + value +"\""

    }
    ByteString (output + "}")
  }

  def mapJsonByteToMap (entity: Entity) (doc:ByteString) : Map[String,String] = {
    var outMap = Map[String,String]()
    var jDoc : JsValue = Json.parse(doc.utf8String)

    for (keyholder<-entity.attributes){
      outMap += (keyholder.attr_name -> (jDoc\keyholder.attr_name).get.toString.replace("\"", ""))
    }
    outMap
  }

  def mapByteToMap (entity: Entity) (doc:String) : Map[String,String] = {
    var outMap = Map[String,String]()
    var jDoc : JsValue = Json.parse(doc)

    for (keyholder<-entity.attributes){
      outMap += (keyholder.attr_name -> (jDoc\keyholder.attr_name).get.toString.replace("\"", ""))
    }
    outMap
  }
}