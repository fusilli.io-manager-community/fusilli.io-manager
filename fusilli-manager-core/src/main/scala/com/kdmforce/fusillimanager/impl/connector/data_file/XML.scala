package com.kdmforce.fusillimanager.impl.connector.data_file

import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.Entity
import com.kdmforce.fusillimanager.impl.connector.DataFile
import javax.xml.xpath.XPathExpression
import akka.stream.scaladsl.{Flow, Source, Framing, JsonFraming}
import akka.util.ByteString
import scala.xml._
import scala.xml.transform._
import scala.collection.immutable
import javax.xml.xpath.XPath
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathExpression
import javax.xml.xpath.XPathFactory
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory
import org.w3c.dom.NodeList
import org.w3c.dom.Document
import java.io.StringReader
import org.slf4j.{Logger, LoggerFactory}


object XML extends DataFile[ByteString, String] {

  private final val log: Logger = LoggerFactory.getLogger("XML")

  override def formatInput(entity: Entity) = {

    log.info("START METHOD FORMATINPUT")

    val outputMap = mapXmlByteToMap(entity) _

    log.info("START XML PARSING")
    Flow[ByteString]
      .map(outputMap)

  }

  override def formatOutput(entity: Entity) = {
    log.info("START METHOD FORMATOUTPUT")

    val header = entity.attributes.map(_.attr_name)

    log.info("START XML PARSING")
    Flow[Map[String, String]]
      .map(mapToXmlString _)

  }


  def mapToXmlString (doc : Map[String, Any]) : ByteString ={
    var output = "<row>"
    var bool = 1
    for ((key,value)<-doc){
      output = output + "<" + key + ">" + value + "</" + key + ">"
    }

    ByteString (output + "</row>")
  }



  def mapXmlByteToMap (entity: Entity) (doc:ByteString) : Map[String,String] = {
    var outMap = Map[String,String]()
    val factory = DocumentBuilderFactory.newInstance
    factory.setNamespaceAware(true)

    val builder = factory.newDocumentBuilder
    val is = new InputSource(new StringReader(doc.utf8String))
    val xDoc: Document  = builder.parse(is)
    val xpathfactory: XPathFactory = XPathFactory.newInstance()
    val xpath: XPath = xpathfactory.newXPath()

    for (keyholder<-entity.attributes){
      val xPathExpression: XPathExpression = xpath.compile((keyholder.attr_name).toString)
      val result = xPathExpression.evaluate(xDoc, XPathConstants.NODESET)

      val nodes = result.asInstanceOf[NodeList]
      var i = 0
      var value = ""
      while( {
        i < nodes.getLength
      }) {
        value = value + nodes.item(i).getFirstChild.getNodeValue  + "-"
        i += 1
      }
      outMap += (keyholder.attr_name -> value.dropRight(1).toString)
    }
    outMap
  }

}
