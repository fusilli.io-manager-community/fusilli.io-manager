package com.kdmforce.fusillimanager.impl.connector.connection

import java.net.InetAddress
import java.nio.file._

import akka.stream.IOResult
import akka.stream.alpakka.ftp.scaladsl.Ftp
import akka.stream.alpakka.ftp.{FtpCredentials, FtpSettings}
import akka.stream.scaladsl.{FileIO,Flow,Sink}
import akka.util.ByteString
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Entity, Repo}
import com.kdmforce.fusillimanager.impl.connector.Connector
import akka.stream.alpakka.file.scaladsl.Directory
import org.slf4j.{Logger, LoggerFactory}
import akka.stream.scaladsl.Source
import scala.concurrent.Future


object FtpDirectoryConnectorWithNames extends Connector[(ByteString,String)]{

  private final val log: Logger = LoggerFactory.getLogger("FTP DIRECTORY WITH NAMES")

  override def getSource(repo: Repo, entity: Entity) = {
    log.info("START METHOD GETSOURCE")

    log.info("START CONNECTION")
    val settings = spawnConnection(repo)

    log.info(s"DIRECTORY CONNECTION ${repo.schema_path.toString}")
    Ftp
      .ls(repo.schema_path, settings)
      .flatMapConcat(ftpFile => {
        if(ftpFile.path.endsWith(entity.entity_type)) {
          Ftp.fromPath(ftpFile.path, settings).reduce((a, b) => a ++ b).map(coupler(_, ftpFile.name.replace(repo.schema_path.toString, "")))
        } else {
          Source.empty
        }
      })
  }

  //da fare
  override def getSink(repo: Repo, entity: Entity) = {

    val file = Paths.get(repo.schema_path + entity.entity_name)

    //FileIO.toPath(file)
    Sink.ignore

  }

  def spawnConnection (repo: Repo): FtpSettings = {
    val credentials = FtpCredentials.create(repo.repo_user, repo.repo_password)

    FtpSettings
      .create(InetAddress.getByName(repo.host))
      .withPort(repo.port)
      .withCredentials(credentials)
      .withBinary(true)
      .withPassiveMode(true)
  }

  def coupler (coso:ByteString, stringa :String):(ByteString,String)={
    (coso,stringa)
  }

}

