package com.kdmforce.fusillimanager.impl.connector.connection


import java.util

import org.apache.kafka.clients.consumer.KafkaConsumer
import java.util.{Properties, Collections}

import scala.collection.JavaConverters._
import akka.actor.ActorSystem
import akka.kafka.ConsumerSettings.createKafkaConsumer
import akka.kafka.ProducerSettings.createKafkaProducer
import akka.kafka.scaladsl.Consumer
import akka.kafka.{ConsumerSettings, ProducerSettings, Subscriptions}
import akka.stream.scaladsl.{Sink, Source}
import akka.util.ByteString
import com.kdmforce.fusillimanager.api.FusilliEndpointSerializer.{Entity, Repo}
import com.kdmforce.fusillimanager.impl.connector.Connector
import org.apache.kafka.clients.consumer.{ConsumerConfig, KafkaConsumer}
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.{JsValue, Json}
import org.apache.kafka.clients.consumer.KafkaConsumer
import scala.util.control.Breaks._




object KafkaConnector extends Connector[Any]{

  private final val log: Logger = LoggerFactory.getLogger("KAFKA")

  override def getSource(repo: Repo, entity: Entity) = {
    log.info("START METHOD GETSOURCE")

    val kafkaConsumerSettings = spawnConsumerScalaConnection(repo)
    log.info(s"START CONNECTION ${kafkaConsumerSettings.toString}")

    val record = createRecordKafkaConsumer(entity, kafkaConsumerSettings)
    log.info(s"RECORD CONSUMER ${record.toString}")

    Source(record)
      .map(ByteString(_))
  }


  override def getSink(repo: Repo, entity: Entity) = {
    log.info("START METHOD GETSINK")

    val kafkaProducerSettings = spawnProducerConnection(repo)
    log.info(s"START CONNECTION ${kafkaProducerSettings.toString}")

    val jsonString = createRecordKafkaProducer(entity, kafkaProducerSettings) _
    log.info(s"RECORD PRODUCER ${jsonString.toString}")

    Sink.foreach(elem => jsonString(elem.asInstanceOf[ByteString]))

  }


  def spawnConsumerScalaConnection(repo: Repo): Properties = {
    val properties = new Properties()
    properties.put("bootstrap.servers", s"${repo.host}:${repo.port}")
    properties.put("group.id", repo.schema_path)
    properties.put("key.deserializer", classOf[StringDeserializer])
    properties.put("value.deserializer", classOf[StringDeserializer])
    properties.put("max.poll.records", Int.MaxValue)
    properties.put("max.partition.fetch.bytes", Int.MaxValue)
    properties.put("fetch.max.bytes", Int.MaxValue)
    properties
  }

  def spawnProducerConnection(repo: Repo): ProducerSettings[String, String] = {
    implicit val system: ActorSystem = ActorSystem()

    val producerConfig = system.settings.config.getConfig("akka.kafka.producer")

    ProducerSettings(producerConfig, new StringSerializer, new StringSerializer)
      .withBootstrapServers(s"${repo.host}:${repo.port}")
  }

  def createRecordKafkaConsumer(entity: Entity, properties: Properties) = {
    val kafkaConsumer = new KafkaConsumer[String, String](properties)
    
    kafkaConsumer.subscribe(Collections.singletonList(entity.entity_name))

    val record = kafkaConsumer.poll(2000).asScala.map(_.value()).toList

    kafkaConsumer.close()

    record

  }

  def createRecordKafkaProducer(entity: Entity, kafkaProducerSettings: ProducerSettings[String, String])(thing: ByteString) = {
    val jsonString = thing.utf8String
    val record = new ProducerRecord[String, String](entity.entity_name, jsonString)
    val kafkaProducer: KafkaProducer[String, String] = createKafkaProducer(kafkaProducerSettings)

    kafkaProducer.send(record)

    kafkaProducer.close()

  }

}


