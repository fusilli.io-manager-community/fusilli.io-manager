package com.kdmforce.fusillimanager.impl.connector


import java.util

import org.apache.kafka.clients.consumer.KafkaConsumer
import java.util.Properties

import scala.collection.JavaConverters._
import akka.actor.ActorSystem
import akka.kafka.ConsumerSettings.createKafkaConsumer
import akka.kafka.ProducerSettings.createKafkaProducer
import akka.kafka.scaladsl.Consumer
import akka.kafka.{ConsumerSettings, ProducerSettings, Subscriptions}
import akka.stream.scaladsl.{Sink, Source}
import akka.util.ByteString
import com.kdmforce.fusillimanager.api.EndpointSerializer.{Entity, Repo}
import com.kdmforce.fusillimanager.impl.connectors.Connector
import org.apache.kafka.clients.consumer.{ConsumerConfig, KafkaConsumer}
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.{JsValue, Json}




object KafkaConnector extends Connector[Any] {

  private final val log: Logger = LoggerFactory.getLogger("")

  override def getSource(repo: Repo, entity: Entity) = {

    val kafkaConsumerSettings = spawnConsumerConnection(repo)

    Consumer.plainSource(kafkaConsumerSettings, Subscriptions.topics(entity.entity_name))
      .map(_.value())
      .map(ByteString(_))

  }


  override def getSink(repo: Repo, entity: Entity) = {

    val kafkaProducerSettings = spawnProducerConnection(repo)

    val jsonString = createElem(entity, kafkaProducerSettings) _

    Sink.foreach(elem => jsonString(elem.asInstanceOf[ByteString]))

  }


  def spawnConsumerConnection(repo: Repo): ConsumerSettings[String, String] = {
    implicit val system = ActorSystem()

    val consumerConfig = system.settings.config.getConfig("akka.kafka.consumer")

    ConsumerSettings(consumerConfig, new StringDeserializer, new StringDeserializer)
      .withBootstrapServers(s"${repo.host}:${repo.port}")
      .withGroupId(repo.schema_path)
      .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
      .withProperty(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "10000")
  }

  def spawnProducerConnection(repo: Repo): ProducerSettings[String, String] = {
    implicit val system: ActorSystem = ActorSystem()

    val producerConfig = system.settings.config.getConfig("akka.kafka.producer")

    ProducerSettings(producerConfig, new StringSerializer, new StringSerializer)
      .withBootstrapServers(s"${repo.host}:${repo.port}")
  }

  def createElem(entity: Entity, kafkaProducerSettings: ProducerSettings[String, String])(thing: ByteString) = {
    val jsonString = thing.utf8String

    val record = new ProducerRecord[String, String](entity.entity_name, jsonString)

    val kafkaProducer: KafkaProducer[String, String] = createKafkaProducer(kafkaProducerSettings)
    kafkaProducer.send(record)

  }

  def mapByteToMap (entity: Entity) (doc:String) : Map[String,String] = {
    var outMap = Map[String,String]()
    var jDoc : JsValue = Json.parse(doc)

    for (keyholder<-entity.attributes){
      outMap += (keyholder.attr_name -> (jDoc\keyholder.attr_name).get.toString.replace("\"", ""))

    }
    outMap
  }
}


